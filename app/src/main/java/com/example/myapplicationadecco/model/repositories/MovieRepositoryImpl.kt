package com.example.myapplicationadecco.model.repositories

import com.example.myapplicationadecco.model.api.ApiAdapter
import com.example.myapplicationadecco.model.api.ApiService
import com.example.myapplicationadecco.model.models.ResponseMovie
import com.example.myapplicationadecco.presenter.MoviePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieRepositoryImpl(var moviePresenter: MoviePresenter) : MovieRepository {

    override fun getListMovie() {
        val apiService: ApiService = ApiAdapter.getMovieService().create(ApiService::class.java)
        val result: Call<ResponseMovie> = apiService.listMovies()

        result.enqueue(object : Callback<ResponseMovie> {
            override fun onResponse(
                call: Call<ResponseMovie>,
                response: Response<ResponseMovie>
            ) {
                response.body()?.let {
                    moviePresenter.successMovies(it.results)
                }
            }
            override fun onFailure(call: Call<ResponseMovie>, t: Throwable) {
                moviePresenter.errorMovies()
            }
        })
    }
}
