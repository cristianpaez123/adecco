package com.example.myapplicationadecco.model.models

data class ResponseMovie(
    val results: List<Movie>
)
