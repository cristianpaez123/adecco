package com.example.myapplicationadecco.model.api

import com.example.myapplicationadecco.model.models.ResponseMovie
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("movie/popular")
    fun listMovies(@Query("api_key") key: String = "7377d2497d16442d2e3935b0979b969b"): Call<ResponseMovie>
}
