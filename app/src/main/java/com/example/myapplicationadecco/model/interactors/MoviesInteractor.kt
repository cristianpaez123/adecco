package com.example.myapplicationadecco.model.interactors

interface MoviesInteractor {
    fun getMoviesAPI()
}
