package com.example.myapplicationadecco.model.interactors

import com.example.myapplicationadecco.model.repositories.MovieRepository
import com.example.myapplicationadecco.model.repositories.MovieRepositoryImpl
import com.example.myapplicationadecco.presenter.MoviePresenter

class MoviesInteractorImpl(var moviePresenter: MoviePresenter) : MoviesInteractor {

    private var movieRepository: MovieRepository = MovieRepositoryImpl(moviePresenter)

    override fun getMoviesAPI() = movieRepository.getListMovie()
}
