package com.example.myapplicationadecco.model.models

import com.google.gson.annotations.SerializedName

data class Movie(
    @SerializedName("poster_path")
    val posterPath: String,
    val overview: String,
    @SerializedName("original_title")
    val originalTitle: String
)
