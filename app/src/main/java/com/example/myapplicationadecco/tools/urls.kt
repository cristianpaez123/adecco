package com.example.myapplicationadecco.tools

const val urlBase = "https://api.themoviedb.org"

const val urlApi = urlBase + "/3/"

const val urlImage = "https://image.tmdb.org/t/p/w500"
