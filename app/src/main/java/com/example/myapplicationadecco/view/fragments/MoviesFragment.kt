package com.example.myapplicationadecco.view.fragments

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplicationadecco.R
import com.example.myapplicationadecco.model.models.Movie
import com.example.myapplicationadecco.presenter.MoviePresenter
import com.example.myapplicationadecco.presenter.MoviesPresenterImpl
import com.example.myapplicationadecco.view.adapters.RecyclerMovieAdapter
import com.example.myapplicationadecco.view.interfaces.MovieView

class MoviesFragment : Fragment(), MovieView {

    // todo: change ProgressDialog
    private var progressDialog: ProgressDialog? = null
    private var retryView: ConstraintLayout? = null
    private var rcvMovies: RecyclerView? = null
    private var btnRetry: Button? = null
    private val movieAdapter: RecyclerMovieAdapter by lazy { RecyclerMovieAdapter() }
    private val moviePresenter: MoviePresenter by lazy { MoviesPresenterImpl(this) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initRecyclerView()

        moviePresenter.onCreatedView()
    }

    private fun initViews() {
        rcvMovies = view?.findViewById(R.id.rcvMovies)
        retryView = view?.findViewById(R.id.retryView)
        progressDialog = ProgressDialog(this.context)
        btnRetry = view?.findViewById(R.id.btnRetry)

        btnRetry?.setOnClickListener {
            moviePresenter.retryClicked()
        }
    }

    private fun initRecyclerView() {
        rcvMovies?.layoutManager = GridLayoutManager(context, 3)
        rcvMovies?.adapter = movieAdapter
        movieAdapter.setOnClickListener(object : RecyclerMovieAdapter.OnClickListener {
            override fun onClickMovie(movie: Movie) {
                goDetailView(movie)
            }
        })
    }

    override fun showMovies(movies: List<Movie>) {
        movieAdapter.setMovies(movies)
    }

    override fun showLoading() {
        progressDialog?.show()
    }

    override fun hideLoading() {
        if (progressDialog?.isShowing == true) progressDialog?.hide()
    }

    override fun showRetry() {
        retryView?.visibility = View.VISIBLE
    }

    override fun hideRetry() {
        retryView?.visibility = View.GONE
    }

    private fun goDetailView(movie: Movie) {
        val action = MoviesFragmentDirections.actionMoviesFragmentToMovieDetailFragment(
            originalTitle = movie.originalTitle,
            overview = movie.overview,
            posterPath = movie.posterPath
        )
        findNavController().navigate(action)
    }
}
