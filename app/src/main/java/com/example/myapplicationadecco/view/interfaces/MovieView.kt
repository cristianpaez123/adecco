package com.example.myapplicationadecco.view.interfaces

import com.example.myapplicationadecco.model.models.Movie

interface MovieView {
    fun showMovies(movies: List<Movie>)
    fun showLoading()
    fun hideLoading()
    fun showRetry()
    fun hideRetry()
}
