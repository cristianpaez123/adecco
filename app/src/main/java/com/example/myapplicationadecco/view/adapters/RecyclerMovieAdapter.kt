package com.example.myapplicationadecco.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplicationadecco.R
import com.example.myapplicationadecco.model.models.Movie
import com.example.myapplicationadecco.tools.urlImage
import com.squareup.picasso.Picasso

class RecyclerMovieAdapter : RecyclerView.Adapter<RecyclerMovieAdapter.MovieHolder>() {

    private var movies: List<Movie> = emptyList()
    private var listener: OnClickListener? = null

    fun setMovies(movies: List<Movie>) {
        this.movies = movies
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.row_movie, parent, false)
        return MovieHolder(view)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        val movie = movies[position]
        holder.render(movie)

        holder.cdvMovie.setOnClickListener {
            listener?.onClickMovie(movie)
        }
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    fun setOnClickListener(listener: OnClickListener) {
        this.listener = listener
    }

    class MovieHolder(v: View) : RecyclerView.ViewHolder(v) {

        var cdvMovie: CardView = v.findViewById(R.id.cdvmovie)
        private var image: ImageView = v.findViewById(R.id.ivMovie)

        fun render(movie: Movie) {
            val image: String = urlImage + movie.posterPath
            Picasso.get().load(image).into(this.image)
        }
    }

    interface OnClickListener {
        fun onClickMovie(movie: Movie)
    }
}
