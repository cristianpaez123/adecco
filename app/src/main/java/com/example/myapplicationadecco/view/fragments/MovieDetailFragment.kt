package com.example.myapplicationadecco.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.example.myapplicationadecco.R
import com.example.myapplicationadecco.tools.urlImage
import com.squareup.picasso.Picasso

class MovieDetailFragment : Fragment() {

    private var args: MovieDetailFragmentArgs? = null

    private var tbMovieDetail: Toolbar? = null
    private var tvTitleToolbar: TextView? = null
    private var tvOverview: TextView? = null
    private var imvMovie: ImageView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        arguments?.let { arguments ->
            args = MovieDetailFragmentArgs.fromBundle(arguments)
        }
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(tbMovieDetail)

        initViews()
        setView()
    }

    private fun initViews() {
        tvTitleToolbar = view?.findViewById(R.id.tvTitleToolbar)
        tbMovieDetail = view?.findViewById(R.id.tbMovieDetail)
        tvOverview = view?.findViewById(R.id.tvOverview)
        imvMovie = view?.findViewById(R.id.imvMovie)

        tbMovieDetail?.setNavigationOnClickListener { activity?.onBackPressed() }
    }

    private fun setView() {
        tbMovieDetail?.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
        tvTitleToolbar?.text = args?.originalTitle
        tvOverview?.text = args?.overview
        Picasso.get().load(urlImage + args?.posterPath).into(imvMovie)
    }
}
