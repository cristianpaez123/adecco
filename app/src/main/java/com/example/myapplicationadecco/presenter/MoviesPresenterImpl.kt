package com.example.myapplicationadecco.presenter

import com.example.myapplicationadecco.model.interactors.MoviesInteractor
import com.example.myapplicationadecco.model.interactors.MoviesInteractorImpl
import com.example.myapplicationadecco.model.models.Movie
import com.example.myapplicationadecco.view.interfaces.MovieView

class MoviesPresenterImpl(var movieView: MovieView) : MoviePresenter {

    private var movieInteractor: MoviesInteractor = MoviesInteractorImpl(this)

    /** use in view */
    override fun onCreatedView() {
        movieView.hideRetry()
        movieView.showLoading()
        movieInteractor.getMoviesAPI()
    }

    override fun retryClicked() {
        movieView.showLoading()
        movieInteractor.getMoviesAPI()
        movieView.hideRetry()
    }

    /** use in model */
    override fun successMovies(movies: List<Movie>) {
        movieView.hideLoading()
        movieView.showMovies(movies)
    }

    override fun errorMovies() {
        movieView.hideLoading()
        movieView.showRetry()
    }
}
