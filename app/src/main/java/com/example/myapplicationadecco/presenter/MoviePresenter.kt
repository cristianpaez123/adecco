package com.example.myapplicationadecco.presenter

import com.example.myapplicationadecco.model.models.Movie

interface MoviePresenter {

    /** use in view */
    fun onCreatedView()
    fun retryClicked()

    /** use in model */
    fun successMovies(movies: List<Movie>)
    fun errorMovies()
}
